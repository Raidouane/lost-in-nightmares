﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuController : MonoBehaviour
{
	public GameObject PauseMenu;
	public GameObject WinMessage;
	public GameObject GameOverMessage;

	public void PauseGame()
	{
		Time.timeScale = 0;
		Cursor.lockState = CursorLockMode.None;
		Cursor.lockState = CursorLockMode.Confined;
		Cursor.visible = true;
		PauseMenu.gameObject.SetActive(true);
		WinMessage.gameObject.SetActive(false);
		GameOverMessage.gameObject.SetActive(false);
	}

	public void ContinueGame()
	{
		Cursor.lockState = CursorLockMode.Locked;
		Cursor.lockState = CursorLockMode.Locked;
		ActiveTime();
		PauseMenu.gameObject.SetActive(false);
	}

	public void GoToMainMenu()
	{
		ActiveTime();
		SceneManager.LoadScene(0);
	}

	public void GoToGameField()
	{
		Cursor.lockState = CursorLockMode.Locked;
		Cursor.visible = false;
		ActiveTime();
		SceneManager.LoadScene(1);
	}

	public void GoToGameFieldLevel2()
	{
		Cursor.lockState = CursorLockMode.Locked;
		Cursor.visible = false;
		ActiveTime();
		SceneManager.LoadScene(2);
	}

	public void Exit()
	{
		#if UNITY_EDITOR
			UnityEditor.EditorApplication.isPlaying = false;
		#else
			Application.Quit();
		#endif
	}

	void FixedUpdate()
	{
		if (Input.GetButtonDown("Cancel"))
		{
			PauseGame();
		}
	}

	private void ActiveTime()
	{
		Time.timeScale = 1;
	}
}
