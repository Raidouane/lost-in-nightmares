﻿using UnityEngine;

public class AnimationManager : MonoBehaviour {

    [Space()]
    public ImportedAnimations animationData;

    [Space()]
    public Animator animator;

    [Space()]
    public GameObject[] copAttach;
    public string copPrefix = "CP-";
    public GameObject[] brawAttach;
    public string brawPrefix = "BR-";

    private AnimationClip[] m_animations;
    private AnimatorOverrideController animatorOverride;
    private int currentAnimation;
    private const string animDefault = "anim_default";
    private const string stateDefault = "State1";

    // Use this for initialization
    void Start ()
    {
        animatorOverride = new AnimatorOverrideController();
        animatorOverride.runtimeAnimatorController = animator.runtimeAnimatorController;
        animator.runtimeAnimatorController = animatorOverride;

        m_animations = animationData.GetAnimations;


	}

    void FixedUpdate()
    {
        bool isForward = Input.GetAxis("Vertical") >= 0;
        if (Input.GetButton("Vertical") && isForward && currentAnimation != 7)
        {
            currentAnimation = 7;
            ChangeAnimation(currentAnimation);
        }
        else if (Input.GetButton("Vertical") && !isForward && currentAnimation != 6)
        {
            currentAnimation = 6;
            ChangeAnimation(currentAnimation);
        }
        else if (Input.GetButton("Jump"))
        {
            currentAnimation = 8;
            ChangeAnimation(currentAnimation);
        }

    }

    public void PlayCurrentAnimation()
    {
        animator.Play(stateDefault, 0,0);
        animator.enabled = true;
    }

    public void StopCurrentAnimation()
    {
        animator.enabled = false;
    }

    public void ChangeAnimation(int index)
    {

        currentAnimation = index;

        animatorOverride[animDefault] = m_animations[index];

        CheckAnimation(m_animations[index].name);

        animator.Play(stateDefault, 0, 0);
        animator.enabled = true;
    }

    public void ChangeAnimation(string anim, int index)
    {
        currentAnimation = index;
        CheckAnimation(m_animations[index].name);
        animatorOverride[anim] = m_animations[index];
        animator.Play(stateDefault, 0, 0);
        animator.enabled = true;
        animator.Rebind();
    }

    public void ChangeAnimation(string anim, AnimationClip clip)
    {
        animatorOverride[anim] = clip;
        CheckAnimation(clip.name);
        animator.Play(stateDefault, 0, 0);
        animator.enabled = true;
        currentAnimation = 0;
    }

    void CheckAnimation(string animName)
    {
        if (animName.Contains(copPrefix))
        {
            for (int i = 0; i < copAttach.Length; i++)
            {
                copAttach[i].SetActive(true);
            }
            for (int i = 0; i < brawAttach.Length; i++)
            {
                brawAttach[i].SetActive(false);
            }
        }
        else if (animName.Contains(brawPrefix))
        {
            for (int i = 0; i < copAttach.Length; i++)
            {
                copAttach[i].SetActive(false);
            }
            for (int i = 0; i < brawAttach.Length; i++)
            {
                brawAttach[i].SetActive(true);
            }
        }
        else
        {
            for (int i = 0; i < copAttach.Length; i++)
            {
                if (copAttach[i].activeSelf)
                    copAttach[i].SetActive(false);
            }
            for (int i = 0; i < brawAttach.Length; i++)
            {
                if (brawAttach[i].activeSelf)
                    brawAttach[i].SetActive(false);
            }
        }

    }
}
