﻿using System;
using UnityEngine;

public class CrawlerController : MonoBehaviour
{
	#region Public Section

	public float	Speed;
	public bool		IsWalkingVertically;

	#endregion


	#region Private Section

	private float _step = 0.1f;

	void FixedUpdate ()
	{
		if (IsWalkingVertically)
		{
			WalkVertically();
			return;
		}

		WalkHorizontally();
	}

	void OnTriggerEnter(Collider other)
	{
		HandleCollisionWithWall(other);
	}

	void WalkVertically()
	{
		Vector3 targetPosition = gameObject.transform.position;
		targetPosition.z = targetPosition.z - (_step * Speed);
		gameObject.transform.position = targetPosition;
	}

	void WalkHorizontally()
	{
		Vector3 targetPosition = gameObject.transform.position;
		targetPosition.x = targetPosition.x + (_step * Speed);
		gameObject.transform.position = targetPosition;
	}

	void HandleCollisionWithWall(Collider other)
	{
		if (!other.gameObject.CompareTag("Wall")) return;

		_step *= -1;
		if (IsWalkingVertically)
		{
			RotateVertically();
			return;
		}

		RotateHorizontally();
	}

	void RotateVertically()
	{
		Quaternion targetRotation = gameObject.transform.rotation;
		targetRotation.y *= -1;
		gameObject.transform.rotation = targetRotation;
	}

	void RotateHorizontally()
	{
		Vector3 targetRotation = gameObject.transform.localEulerAngles;
		if (Math.Abs(targetRotation.y - 180.0f) < 2f)
		{
			targetRotation.y = 0;
		}
		else
		{
			targetRotation.y = 180f;
		}
		gameObject.transform.rotation = Quaternion.Euler(targetRotation);
	}

	#endregion
}
