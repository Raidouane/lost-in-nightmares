﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {

	#region Public Section

	public Transform 	Player;
	public float 		MouseSensitivity = 5.0f;

	#endregion

	#region Private Section

	private float _xAxisClamp = 0.0f;

	private void Awake()
	{
		Cursor.lockState = CursorLockMode.Locked;
	}

	private void Update()
	{
		RotateCamera();
	}

	private void RotateCamera()
	{
		float mouseX = Input.GetAxis("Mouse X");
		float mouseY = Input.GetAxis("Mouse Y");
		float rotAmountX = mouseX * MouseSensitivity;
		float rotAmountY = mouseY * MouseSensitivity;
		const float limitClamp = 90.0f;

		_xAxisClamp -= rotAmountY;
		Vector3 targetRotCam = transform.rotation.eulerAngles;
		Vector3 targetRotBody = Player.rotation.eulerAngles;
		targetRotCam.x -= rotAmountY;
		targetRotCam.z = 0;
		targetRotBody.y += rotAmountX;
		if (_xAxisClamp > limitClamp)
		{
			_xAxisClamp = limitClamp;
			targetRotCam.x = limitClamp;
		}
		else if (_xAxisClamp < -limitClamp)
		{
			_xAxisClamp = -limitClamp;
			targetRotCam.x = 360 - limitClamp;
		}

		transform.rotation = Quaternion.Euler(targetRotCam);
		Player.rotation = Quaternion.Euler(targetRotBody);
	}

	#endregion
}
