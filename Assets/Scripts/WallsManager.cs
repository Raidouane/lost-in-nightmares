﻿using UnityEngine;

public enum Sides
{
	North,
	South,
	West,
	East
}

public class WallsManager : MonoBehaviour {

	#region Public Section

	public Sides Side;
	public GameObject Ground;

	#endregion

	#region Private Section

	void Start ()
	{
		if (!Ground) return;

		switch (Side)
		{
			case Sides.North:
			{
				transform.position = new Vector3(
					Ground.transform.position.x,
					Ground.transform.position.y,
					5 * Ground.transform.localScale.z + Ground.transform.position.z
				);
				break;
			}
			case Sides.South:
			{
				transform.position = new Vector3(
					Ground.transform.position.x,
					Ground.transform.position.y,
					-(5 * Ground.transform.localScale.z + Ground.transform.position.z)
				);
				break;
			}
			case Sides.West:
			{
				transform.position = new Vector3(
					-(5 * Ground.transform.localScale.x + Ground.transform.position.x),
					Ground.transform.position.y,
					Ground.transform.position.z
				);
				break;
			}
			case Sides.East:
			{
				transform.position = new Vector3(
					5 * Ground.transform.localScale.x + Ground.transform.position.x,
					Ground.transform.position.y,
					Ground.transform.position.z
				);
				break;
			}
		}
	}

	#endregion
}
