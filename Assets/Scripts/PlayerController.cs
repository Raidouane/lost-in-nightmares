﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
	#region Public Section

	public float		Speed;
	public Text			CounterText;
	public Text			WinMessage;
	public GameObject	Character;
	public Text			GameOverMessage;
	public float	 	JumpSpeed = 8.0f;
	public float 		Gravity = 20.0f;

	#endregion


	#region Private Section

	private uint					_counter;
	private Vector3 				_moveDirection = Vector3.zero;
	private CharacterController 	_controller;

	private void Start()
	{
		if (Math.Abs(Speed) < 0.1f)
		{
			Speed = 5.0f;
			_counter = 0;
			SetCounterText();
		}
		_controller = GetComponent<CharacterController>();

		gameObject.transform.position = new Vector3(0, 5, 0);
	}

	void FixedUpdate ()
	{
		if (GameOverMessage.gameObject.activeSelf) return;

		if (_controller.isGrounded)
		{
			_moveDirection = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
			_moveDirection = transform.TransformDirection(_moveDirection);
			_moveDirection = _moveDirection * Speed;

			if (Input.GetButton("Jump"))
			{
				_moveDirection.y = JumpSpeed;
			}
		}

		_moveDirection.y = _moveDirection.y - (Gravity * Time.deltaTime);

		_controller.Move(_moveDirection * Time.deltaTime);
	}

	void OnTriggerEnter(Collider other)
	{
		HandleCollisionWithPickUp(other);
		HandleCollisionWithEnemyLvl1(other);
		HandleCollisionWithTrapLvl1(other);
	}

	void HandleCollisionWithPickUp(Collider other)
	{
		if (!other.gameObject.CompareTag("PickUp")) return;

		other.gameObject.SetActive(false);
		_counter++;
		SetCounterText();
		if (_counter >= 11)
		{
			WinMessage.gameObject.SetActive(true);
		}
	}

	void HandleCollisionWithEnemyLvl1(Collider other)
	{
		if (!other.gameObject.CompareTag("EnemyLvl1")) return;


		Character.SetActive(false);
		GameOverMessage.gameObject.SetActive(true);
	}

	void HandleCollisionWithTrapLvl1(Collider other)
	{
		if (!other.gameObject.CompareTag("TrapLvl1")) return;

		Character.SetActive(false);
		GameOverMessage.gameObject.SetActive(true);
	}

	void SetCounterText()
	{
		CounterText.text = "Count: " + _counter;
	}

	#endregion
}
